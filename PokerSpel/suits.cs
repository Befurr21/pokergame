﻿namespace PokerSpel
{
    public enum Suit
    {
        clubs,
        hearts,
        diamonds,
        spades
    }
}