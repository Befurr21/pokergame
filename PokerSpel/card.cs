﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerSpel
{
    class card
    {
        public Value Value { get; set; }
        public Suit Suit { get; set; }
        public Bitmap Face { get; set; }
        public Bitmap Back { get; set; }
    }
}
