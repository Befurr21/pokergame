﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace PokerSpel
{
    public partial class PokerGame : Form
    {
        int credits = 100;
        public int counter = 0;
        public bool flipped = false;
        int countCards = 1;

        UserControl1[] Cards = new UserControl1[100];
        int ucIndex = 0;

        public PokerGame()
        {
            InitializeComponent();
        }

        private void PokerGame_Load(object sender, EventArgs e)
        {
            lblCreditsDAYE.Text = Convert.ToString(credits);
            nudBetDAYE.Value = 2;
            nudBetDAYE.Value = 1;
        }

        private void nudBetDAYE_ValueChanged(object sender, EventArgs e)
        {
            if (nudBetDAYE.Value == 1)
            {
                pnlRoyaleFlush1.BackColor = Color.DarkRed;
                pnlStraightFlush1.BackColor = Color.DarkRed;
                pnl4ofaKind1.BackColor = Color.DarkRed;
                pnlFullHouse1.BackColor = Color.DarkRed;
                pnlFlush1.BackColor = Color.DarkRed;
                pnlStraight1.BackColor = Color.DarkRed;
                pnl3ofaKind1.BackColor = Color.DarkRed;
                pnlTwopairs1.BackColor = Color.DarkRed;
                pnlJackorBetter1.BackColor = Color.DarkRed;

                pnlRoyaleFlush2.BackColor = Color.Black;
                pnlStraightFlush2.BackColor = Color.Black;
                pnl4ofaKind2.BackColor = Color.Black;
                pnlFullHouse2.BackColor = Color.Black;
                pnlFlush2.BackColor = Color.Black;
                pnlStraight2.BackColor = Color.Black;
                pnl3ofaKind2.BackColor = Color.Black;
                pnlTwopairs2.BackColor = Color.Black;
                pnlJackorBetter2.BackColor = Color.Black;

            }
            else if (nudBetDAYE.Value == 2)
            {
                pnlRoyaleFlush2.BackColor = Color.DarkRed;
                pnlStraightFlush2.BackColor = Color.DarkRed;
                pnl4ofaKind2.BackColor = Color.DarkRed;
                pnlFullHouse2.BackColor = Color.DarkRed;
                pnlFlush2.BackColor = Color.DarkRed;
                pnlStraight2.BackColor = Color.DarkRed;
                pnl3ofaKind2.BackColor = Color.DarkRed;
                pnlTwopairs2.BackColor = Color.DarkRed;
                pnlJackorBetter2.BackColor = Color.DarkRed;

                pnlRoyaleFlush1.BackColor = Color.Black;
                pnlStraightFlush1.BackColor = Color.Black;
                pnl4ofaKind1.BackColor = Color.Black;
                pnlFullHouse1.BackColor = Color.Black;
                pnlFlush1.BackColor = Color.Black;
                pnlStraight1.BackColor = Color.Black;
                pnl3ofaKind1.BackColor = Color.Black;
                pnlTwopairs1.BackColor = Color.Black;
                pnlJackorBetter1.BackColor = Color.Black;

                pnlRoyaleFlush3.BackColor = Color.Black;
                pnlStraightFlush3.BackColor = Color.Black;
                pnl4ofaKind3.BackColor = Color.Black;
                pnlFullHouse3.BackColor = Color.Black;
                pnlFlush3.BackColor = Color.Black;
                pnlStraight3.BackColor = Color.Black;
                pnl3ofaKind3.BackColor = Color.Black;
                pnlTwopairs3.BackColor = Color.Black;
                pnlJackorBetter3.BackColor = Color.Black;
            }
            else if (nudBetDAYE.Value == 3)
            {
                pnlRoyaleFlush3.BackColor = Color.DarkRed;
                pnlStraightFlush3.BackColor = Color.DarkRed;
                pnl4ofaKind3.BackColor = Color.DarkRed;
                pnlFullHouse3.BackColor = Color.DarkRed;
                pnlFlush3.BackColor = Color.DarkRed;
                pnlStraight3.BackColor = Color.DarkRed;
                pnl3ofaKind3.BackColor = Color.DarkRed;
                pnlTwopairs3.BackColor = Color.DarkRed;
                pnlJackorBetter3.BackColor = Color.DarkRed;

                pnlRoyaleFlush2.BackColor = Color.Black;
                pnlStraightFlush2.BackColor = Color.Black;
                pnl4ofaKind2.BackColor = Color.Black;
                pnlFullHouse2.BackColor = Color.Black;
                pnlFlush2.BackColor = Color.Black;
                pnlStraight2.BackColor = Color.Black;
                pnl3ofaKind2.BackColor = Color.Black;
                pnlTwopairs2.BackColor = Color.Black;
                pnlJackorBetter2.BackColor = Color.Black;

                pnlRoyaleFlush4.BackColor = Color.Black;
                pnlStraightFlush4.BackColor = Color.Black;
                pnl4ofaKind4.BackColor = Color.Black;
                pnlFullHouse4.BackColor = Color.Black;
                pnlFlush4.BackColor = Color.Black;
                pnlStraight4.BackColor = Color.Black;
                pnl3ofaKind4.BackColor = Color.Black;
                pnlTwopairs4.BackColor = Color.Black;
                pnlJackorBetter4.BackColor = Color.Black;
            }
            else if (nudBetDAYE.Value == 4)
            {
                pnlRoyaleFlush4.BackColor = Color.DarkRed;
                pnlStraightFlush4.BackColor = Color.DarkRed;
                pnl4ofaKind4.BackColor = Color.DarkRed;
                pnlFullHouse4.BackColor = Color.DarkRed;
                pnlFlush4.BackColor = Color.DarkRed;
                pnlStraight4.BackColor = Color.DarkRed;
                pnl3ofaKind4.BackColor = Color.DarkRed;
                pnlTwopairs4.BackColor = Color.DarkRed;
                pnlJackorBetter4.BackColor = Color.DarkRed;

                pnlRoyaleFlush3.BackColor = Color.Black;
                pnlStraightFlush3.BackColor = Color.Black;
                pnl4ofaKind3.BackColor = Color.Black;
                pnlFullHouse3.BackColor = Color.Black;
                pnlFlush3.BackColor = Color.Black;
                pnlStraight3.BackColor = Color.Black;
                pnl3ofaKind3.BackColor = Color.Black;
                pnlTwopairs3.BackColor = Color.Black;
                pnlJackorBetter3.BackColor = Color.Black;

                pnlRoyaleFlush5.BackColor = Color.Black;
                pnlStraightFlush5.BackColor = Color.Black;
                pnl4ofaKind5.BackColor = Color.Black;
                pnlFullHouse5.BackColor = Color.Black;
                pnlFlush5.BackColor = Color.Black;
                pnlStraight5.BackColor = Color.Black;
                pnl3ofaKind5.BackColor = Color.Black;
                pnlTwopairs5.BackColor = Color.Black;
                pnlJackorBetter5.BackColor = Color.Black;
            }
            else if (nudBetDAYE.Value == 5)
            {
                pnlRoyaleFlush5.BackColor = Color.DarkRed;
                pnlStraightFlush5.BackColor = Color.DarkRed;
                pnl4ofaKind5.BackColor = Color.DarkRed;
                pnlFullHouse5.BackColor = Color.DarkRed;
                pnlFlush5.BackColor = Color.DarkRed;
                pnlStraight5.BackColor = Color.DarkRed;
                pnl3ofaKind5.BackColor = Color.DarkRed;
                pnlTwopairs5.BackColor = Color.DarkRed;
                pnlJackorBetter5.BackColor = Color.DarkRed;

                pnlRoyaleFlush4.BackColor = Color.Black;
                pnlStraightFlush4.BackColor = Color.Black;
                pnl4ofaKind4.BackColor = Color.Black;
                pnlFullHouse4.BackColor = Color.Black;
                pnlFlush4.BackColor = Color.Black;
                pnlStraight4.BackColor = Color.Black;
                pnl3ofaKind4.BackColor = Color.Black;
                pnlTwopairs4.BackColor = Color.Black;
                pnlJackorBetter4.BackColor = Color.Black;
            }
        }

        private void btnActionDAYE_Click(object sender, EventArgs e)
        {
            createUc();

            //counter = 180;
            //TimerFlip.Start();
        }

        private void TimerFlip_Tick(object sender, EventArgs e)
        {
            while (counter != 0 && flipped == false)
            { 
                counter = counter - 12;
                //lblCreditsDAYE.Text = Convert.ToString(counter);
                pbCardholder1DAYE.Width = counter;
                pbCardholder1DAYE.Left = pbCardholder1DAYE.Left + 6;
                return;
            }
            while (counter != 180)
            {
                flipped = true;
                pbCardholder1DAYE.Image = Image.FromFile(@"../../Image/Cards/Clubs 1.png");
                counter = counter + 12;
                //lblCreditsDAYE.Text = Convert.ToString(counter);
                pbCardholder1DAYE.Width = counter;
                pbCardholder1DAYE.Left = pbCardholder1DAYE.Left - 6;
                return;
            }
        }

        private void createUc()
        {
            TimerDeal.Start();        
        }

        private void TimerDeal_Tick(object sender, EventArgs e)
        {
            Cards[ucIndex] = new UserControl1();
            Cards[ucIndex].Parent = this;
            

            switch (countCards)
            {
                case 1:
                    
                    pbCardholder1DAYE.Controls.Add(Cards[ucIndex]);
                    countCards++;
                    label57.Text = Convert.ToString(UserControl1.dubbelen);
                    break;
                case 2:
                    pbCardholder2DAYE.Controls.Add(Cards[ucIndex]);
                    countCards++;
                    label57.Text = Convert.ToString(UserControl1.dubbelen);
                    break;
                case 3:
                    pbCardholder3DAYE.Controls.Add(Cards[ucIndex]);
                    countCards++;
                    label57.Text = Convert.ToString(UserControl1.dubbelen);
                    break;
                case 4:
                    pbCardholder4DAYE.Controls.Add(Cards[ucIndex]);
                    countCards++;
                    label57.Text = Convert.ToString(UserControl1.dubbelen);
                    break;
                case 5:
                    pbCardholder5DAYE.Controls.Add(Cards[ucIndex]);
                    countCards++;
                    label57.Text = Convert.ToString(UserControl1.dubbelen);
                    TimerDeal.Stop();
                    break;
            }
            ucIndex++;
            UserControl1.ucIndex = ucIndex;
            //Cards[ucIndex].ucIndex = ucIndex;
        }
    }
}
