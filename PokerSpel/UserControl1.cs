﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokerSpel
{
    public partial class UserControl1 : UserControl
    {
        public int randomInt;
        public int counter = 180;
        public bool flipped = false;
        public bool holdBack = false;
        public static int ucIndex;

        public static int[] cards = new int[100];
        public static int x;
        public static int dubbelen;


        public UserControl1()
        {
            InitializeComponent();
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
            TimerFlip.Start();
        }

        private void randomCard()
        {
            Random rnd = new Random();
            randomInt = rnd.Next(1, 14);
            if (cards.Contains(randomInt))
            {
                dubbelen++;
                randomCard();
            }
            else
            {
                cards[x] = randomInt;
                x++;

                switch (randomInt)
                {
                    case 1:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 1.png");
                        break;
                    case 2:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 2.png");
                        break;
                    case 3:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 3.png");
                        break;
                    case 4:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 4.png");
                        break;
                    case 5:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 5.png");
                        break;
                    case 6:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 6.png");
                        break;
                    case 7:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 7.png");
                        break;
                    case 8:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 8.png");
                        break;
                    case 9:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 9.png");
                        break;
                    case 10:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 10.png");
                        break;
                    case 11:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 11.png");
                        break;
                    case 12:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 12.png");
                        break;
                    case 13:
                        pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Clubs 13.png");
                        break;
                        //case 14:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 1.png");
                        //    break;
                        //case 15:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 2.png");
                        //    break;
                        //case 16:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 3.png");
                        //    break;
                        //case 17:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 4.png");
                        //    break;
                        //case 18:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 5.png");
                        //    break;
                        //case 19:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 6.png");
                        //    break;
                        //case 20:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 7.png");
                        //    break;
                        //case 21:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 8.png");
                        //    break;
                        //case 22:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 9.png");
                        //    break;
                        //case 23:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 10.png");
                        //    break;
                        //case 24:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 11.png");
                        //    break;
                        //case 25:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 12.png");
                        //    break;
                        //case 26:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Diamonds 13.png");
                        //    break;
                        //case 27:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 1.png");
                        //    break;
                        //case 28:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 2.png");
                        //    break;
                        //case 29:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 3.png");
                        //    break;
                        //case 30:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 4.png");
                        //    break;
                        //case 31:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 5.png");
                        //    break;
                        //case 32:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 6.png");
                        //    break;
                        //case 33:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 7.png");
                        //    break;
                        //case 34:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 8.png");
                        //    break;
                        //case 35:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 9.png");
                        //    break;
                        //case 36:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 10.png");
                        //    break;
                        //case 37:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 11.png");
                        //    break;
                        //case 38:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 12.png");
                        //    break;
                        //case 39:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Hearts 13.png");
                        //    break;
                        //case 40:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 1.png");
                        //    break;
                        //case 41:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 2.png");
                        //    break;
                        //case 42:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 3.png");
                        //    break;
                        //case 43:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 4.png");
                        //    break;
                        //case 44:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 5.png");
                        //    break;
                        //case 45:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 6.png");
                        //    break;
                        //case 46:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 7.png");
                        //    break;
                        //case 47:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 8.png");
                        //    break;
                        //case 48:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 9.png");
                        //    break;
                        //case 49:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 10.png");
                        //    break;
                        //case 50:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 11.png");
                        //    break;
                        //case 51:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 12.png");
                        //    break;
                        //case 52:
                        //    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/Spades 13.png");
                        //    break;
                }
            }
            //Geen code zetten. Anders rotzooid het met de if{}.
        }
            

        private void TimerFlip_Tick(object sender, EventArgs e)
        {
            while (counter != 0 && flipped == false)
            {
                if (holdBack == false)
                { 
                    pbPlaceHolderUC.Image = Image.FromFile(@"../../Image/Cards/_Back.png");
                }
                holdBack = true;
                counter = counter - 12;
                pbPlaceHolderUC.Width = counter;
                pbPlaceHolderUC.Left = pbPlaceHolderUC.Left + 6;
                return;
            }
            while (counter != 180)
            {
                if (flipped == false)
                {
                    randomCard();
                }
                flipped = true;
                counter = counter + 12;
                pbPlaceHolderUC.Width = counter;
                pbPlaceHolderUC.Left = pbPlaceHolderUC.Left - 6;
                return;
            }
        }
    }
}
