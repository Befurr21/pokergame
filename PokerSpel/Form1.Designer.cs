﻿namespace PokerSpel
{
    partial class PokerGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PokerGame));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlJackorBetter = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlTwopairs = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.pnl3ofaKind = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlStraight = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlFlush = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlFullHouse = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pnl4ofaKind = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlStraightFlush = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlJackorBetter5 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.pnlJackorBetter4 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.pnlJackorBetter2 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.pnlJackorBetter3 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.pnlJackorBetter1 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.pnlTwopairs5 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.pnlTwopairs4 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.pnlTwopairs2 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.pnlTwopairs3 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.pnlTwopairs1 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.pnl3ofaKind5 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.pnl3ofaKind4 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.pnl3ofaKind2 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.pnl3ofaKind3 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.pnl3ofaKind1 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.pnlStraight5 = new System.Windows.Forms.Panel();
            this.label52 = new System.Windows.Forms.Label();
            this.pnlStraight4 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.pnlStraight2 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.pnlStraight3 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.pnlStraight1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlFlush5 = new System.Windows.Forms.Panel();
            this.label51 = new System.Windows.Forms.Label();
            this.pnlFlush4 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.pnlFlush2 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.pnlFlush3 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.pnlFlush1 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.pnlFullHouse5 = new System.Windows.Forms.Panel();
            this.label50 = new System.Windows.Forms.Label();
            this.pnlFullHouse4 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.pnlFullHouse2 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.pnlFullHouse3 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.pnlFullHouse1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.pnl4ofaKind5 = new System.Windows.Forms.Panel();
            this.label49 = new System.Windows.Forms.Label();
            this.pnl4ofaKind4 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.pnl4ofaKind2 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.pnl4ofaKind3 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.pnl4ofaKind1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.pnlStraightFlush5 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.pnlStraightFlush4 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.pnlStraightFlush2 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.pnlStraightFlush3 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.pnlStraightFlush1 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlRoyaleFlush5 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.pnlRoyaleFlush4 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.pnlRoyaleFlush2 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.pnlRoyaleFlush3 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.pnlRoyaleFlush1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlRoyaleFlush = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnActionDAYE = new System.Windows.Forms.Button();
            this.nudBetDAYE = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCreditsDAYE = new System.Windows.Forms.Label();
            this.TimerDeal = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbCardholder5DAYE = new System.Windows.Forms.PictureBox();
            this.pbCardholder4DAYE = new System.Windows.Forms.PictureBox();
            this.pbCardholder3DAYE = new System.Windows.Forms.PictureBox();
            this.pbCardholder2DAYE = new System.Windows.Forms.PictureBox();
            this.pbCardholder1DAYE = new System.Windows.Forms.PictureBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlJackorBetter.SuspendLayout();
            this.pnlTwopairs.SuspendLayout();
            this.pnl3ofaKind.SuspendLayout();
            this.pnlStraight.SuspendLayout();
            this.pnlFlush.SuspendLayout();
            this.pnlFullHouse.SuspendLayout();
            this.pnl4ofaKind.SuspendLayout();
            this.pnlStraightFlush.SuspendLayout();
            this.pnlJackorBetter5.SuspendLayout();
            this.pnlJackorBetter4.SuspendLayout();
            this.pnlJackorBetter2.SuspendLayout();
            this.pnlJackorBetter3.SuspendLayout();
            this.pnlJackorBetter1.SuspendLayout();
            this.pnlTwopairs5.SuspendLayout();
            this.pnlTwopairs4.SuspendLayout();
            this.pnlTwopairs2.SuspendLayout();
            this.pnlTwopairs3.SuspendLayout();
            this.pnlTwopairs1.SuspendLayout();
            this.pnl3ofaKind5.SuspendLayout();
            this.pnl3ofaKind4.SuspendLayout();
            this.pnl3ofaKind2.SuspendLayout();
            this.pnl3ofaKind3.SuspendLayout();
            this.pnl3ofaKind1.SuspendLayout();
            this.pnlStraight5.SuspendLayout();
            this.pnlStraight4.SuspendLayout();
            this.pnlStraight2.SuspendLayout();
            this.pnlStraight3.SuspendLayout();
            this.pnlStraight1.SuspendLayout();
            this.pnlFlush5.SuspendLayout();
            this.pnlFlush4.SuspendLayout();
            this.pnlFlush2.SuspendLayout();
            this.pnlFlush3.SuspendLayout();
            this.pnlFlush1.SuspendLayout();
            this.pnlFullHouse5.SuspendLayout();
            this.pnlFullHouse4.SuspendLayout();
            this.pnlFullHouse2.SuspendLayout();
            this.pnlFullHouse3.SuspendLayout();
            this.pnlFullHouse1.SuspendLayout();
            this.pnl4ofaKind5.SuspendLayout();
            this.pnl4ofaKind4.SuspendLayout();
            this.pnl4ofaKind2.SuspendLayout();
            this.pnl4ofaKind3.SuspendLayout();
            this.pnl4ofaKind1.SuspendLayout();
            this.pnlStraightFlush5.SuspendLayout();
            this.pnlStraightFlush4.SuspendLayout();
            this.pnlStraightFlush2.SuspendLayout();
            this.pnlStraightFlush3.SuspendLayout();
            this.pnlStraightFlush1.SuspendLayout();
            this.pnlRoyaleFlush5.SuspendLayout();
            this.pnlRoyaleFlush4.SuspendLayout();
            this.pnlRoyaleFlush2.SuspendLayout();
            this.pnlRoyaleFlush3.SuspendLayout();
            this.pnlRoyaleFlush1.SuspendLayout();
            this.pnlRoyaleFlush.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBetDAYE)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder5DAYE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder4DAYE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder3DAYE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder2DAYE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder1DAYE)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.Khaki;
            this.panel1.Controls.Add(this.pnlJackorBetter);
            this.panel1.Controls.Add(this.pnlTwopairs);
            this.panel1.Controls.Add(this.pnl3ofaKind);
            this.panel1.Controls.Add(this.pnlStraight);
            this.panel1.Controls.Add(this.pnlFlush);
            this.panel1.Controls.Add(this.pnlFullHouse);
            this.panel1.Controls.Add(this.pnl4ofaKind);
            this.panel1.Controls.Add(this.pnlStraightFlush);
            this.panel1.Controls.Add(this.pnlJackorBetter5);
            this.panel1.Controls.Add(this.pnlJackorBetter4);
            this.panel1.Controls.Add(this.pnlJackorBetter2);
            this.panel1.Controls.Add(this.pnlJackorBetter3);
            this.panel1.Controls.Add(this.pnlJackorBetter1);
            this.panel1.Controls.Add(this.pnlTwopairs5);
            this.panel1.Controls.Add(this.pnlTwopairs4);
            this.panel1.Controls.Add(this.pnlTwopairs2);
            this.panel1.Controls.Add(this.pnlTwopairs3);
            this.panel1.Controls.Add(this.pnlTwopairs1);
            this.panel1.Controls.Add(this.pnl3ofaKind5);
            this.panel1.Controls.Add(this.pnl3ofaKind4);
            this.panel1.Controls.Add(this.pnl3ofaKind2);
            this.panel1.Controls.Add(this.pnl3ofaKind3);
            this.panel1.Controls.Add(this.pnl3ofaKind1);
            this.panel1.Controls.Add(this.pnlStraight5);
            this.panel1.Controls.Add(this.pnlStraight4);
            this.panel1.Controls.Add(this.pnlStraight2);
            this.panel1.Controls.Add(this.pnlStraight3);
            this.panel1.Controls.Add(this.pnlStraight1);
            this.panel1.Controls.Add(this.pnlFlush5);
            this.panel1.Controls.Add(this.pnlFlush4);
            this.panel1.Controls.Add(this.pnlFlush2);
            this.panel1.Controls.Add(this.pnlFlush3);
            this.panel1.Controls.Add(this.pnlFlush1);
            this.panel1.Controls.Add(this.pnlFullHouse5);
            this.panel1.Controls.Add(this.pnlFullHouse4);
            this.panel1.Controls.Add(this.pnlFullHouse2);
            this.panel1.Controls.Add(this.pnlFullHouse3);
            this.panel1.Controls.Add(this.pnlFullHouse1);
            this.panel1.Controls.Add(this.pnl4ofaKind5);
            this.panel1.Controls.Add(this.pnl4ofaKind4);
            this.panel1.Controls.Add(this.pnl4ofaKind2);
            this.panel1.Controls.Add(this.pnl4ofaKind3);
            this.panel1.Controls.Add(this.pnl4ofaKind1);
            this.panel1.Controls.Add(this.pnlStraightFlush5);
            this.panel1.Controls.Add(this.pnlStraightFlush4);
            this.panel1.Controls.Add(this.pnlStraightFlush2);
            this.panel1.Controls.Add(this.pnlStraightFlush3);
            this.panel1.Controls.Add(this.pnlStraightFlush1);
            this.panel1.Controls.Add(this.pnlRoyaleFlush5);
            this.panel1.Controls.Add(this.pnlRoyaleFlush4);
            this.panel1.Controls.Add(this.pnlRoyaleFlush2);
            this.panel1.Controls.Add(this.pnlRoyaleFlush3);
            this.panel1.Controls.Add(this.pnlRoyaleFlush1);
            this.panel1.Controls.Add(this.pnlRoyaleFlush);
            this.panel1.Location = new System.Drawing.Point(342, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(616, 186);
            this.panel1.TabIndex = 0;
            // 
            // pnlJackorBetter
            // 
            this.pnlJackorBetter.BackColor = System.Drawing.Color.Black;
            this.pnlJackorBetter.Controls.Add(this.label10);
            this.pnlJackorBetter.Location = new System.Drawing.Point(3, 163);
            this.pnlJackorBetter.Name = "pnlJackorBetter";
            this.pnlJackorBetter.Size = new System.Drawing.Size(150, 20);
            this.pnlJackorBetter.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Khaki;
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(146, 16);
            this.label10.TabIndex = 0;
            this.label10.Text = "JACKS OR BETTER";
            // 
            // pnlTwopairs
            // 
            this.pnlTwopairs.BackColor = System.Drawing.Color.Black;
            this.pnlTwopairs.Controls.Add(this.label9);
            this.pnlTwopairs.Location = new System.Drawing.Point(3, 143);
            this.pnlTwopairs.Name = "pnlTwopairs";
            this.pnlTwopairs.Size = new System.Drawing.Size(150, 20);
            this.pnlTwopairs.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Khaki;
            this.label9.Location = new System.Drawing.Point(3, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "TWO PAIRS";
            // 
            // pnl3ofaKind
            // 
            this.pnl3ofaKind.BackColor = System.Drawing.Color.Black;
            this.pnl3ofaKind.Controls.Add(this.label8);
            this.pnl3ofaKind.Location = new System.Drawing.Point(3, 123);
            this.pnl3ofaKind.Name = "pnl3ofaKind";
            this.pnl3ofaKind.Size = new System.Drawing.Size(150, 20);
            this.pnl3ofaKind.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Khaki;
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "3 OF A KIND";
            // 
            // pnlStraight
            // 
            this.pnlStraight.BackColor = System.Drawing.Color.Black;
            this.pnlStraight.Controls.Add(this.label7);
            this.pnlStraight.Location = new System.Drawing.Point(3, 103);
            this.pnlStraight.Name = "pnlStraight";
            this.pnlStraight.Size = new System.Drawing.Size(150, 20);
            this.pnlStraight.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Khaki;
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 16);
            this.label7.TabIndex = 0;
            this.label7.Text = "STRAIGHT";
            // 
            // pnlFlush
            // 
            this.pnlFlush.BackColor = System.Drawing.Color.Black;
            this.pnlFlush.Controls.Add(this.label6);
            this.pnlFlush.Location = new System.Drawing.Point(3, 83);
            this.pnlFlush.Name = "pnlFlush";
            this.pnlFlush.Size = new System.Drawing.Size(150, 20);
            this.pnlFlush.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Khaki;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "FLUSH";
            // 
            // pnlFullHouse
            // 
            this.pnlFullHouse.BackColor = System.Drawing.Color.Black;
            this.pnlFullHouse.Controls.Add(this.label5);
            this.pnlFullHouse.Location = new System.Drawing.Point(3, 63);
            this.pnlFullHouse.Name = "pnlFullHouse";
            this.pnlFullHouse.Size = new System.Drawing.Size(150, 20);
            this.pnlFullHouse.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Khaki;
            this.label5.Location = new System.Drawing.Point(3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "FULL HOUSE";
            // 
            // pnl4ofaKind
            // 
            this.pnl4ofaKind.BackColor = System.Drawing.Color.Black;
            this.pnl4ofaKind.Controls.Add(this.label4);
            this.pnl4ofaKind.Location = new System.Drawing.Point(3, 43);
            this.pnl4ofaKind.Name = "pnl4ofaKind";
            this.pnl4ofaKind.Size = new System.Drawing.Size(150, 20);
            this.pnl4ofaKind.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Khaki;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "4 OF A KIND";
            // 
            // pnlStraightFlush
            // 
            this.pnlStraightFlush.BackColor = System.Drawing.Color.Black;
            this.pnlStraightFlush.Controls.Add(this.label3);
            this.pnlStraightFlush.Location = new System.Drawing.Point(3, 23);
            this.pnlStraightFlush.Name = "pnlStraightFlush";
            this.pnlStraightFlush.Size = new System.Drawing.Size(150, 20);
            this.pnlStraightFlush.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Khaki;
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "STRAIGHT FLUSH";
            // 
            // pnlJackorBetter5
            // 
            this.pnlJackorBetter5.BackColor = System.Drawing.Color.Black;
            this.pnlJackorBetter5.Controls.Add(this.label55);
            this.pnlJackorBetter5.Location = new System.Drawing.Point(523, 163);
            this.pnlJackorBetter5.Name = "pnlJackorBetter5";
            this.pnlJackorBetter5.Size = new System.Drawing.Size(90, 20);
            this.pnlJackorBetter5.TabIndex = 5;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Cursor = System.Windows.Forms.Cursors.Default;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Khaki;
            this.label55.Location = new System.Drawing.Point(37, 3);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(16, 16);
            this.label55.TabIndex = 0;
            this.label55.Text = "5";
            // 
            // pnlJackorBetter4
            // 
            this.pnlJackorBetter4.BackColor = System.Drawing.Color.Black;
            this.pnlJackorBetter4.Controls.Add(this.label46);
            this.pnlJackorBetter4.Location = new System.Drawing.Point(431, 163);
            this.pnlJackorBetter4.Name = "pnlJackorBetter4";
            this.pnlJackorBetter4.Size = new System.Drawing.Size(90, 20);
            this.pnlJackorBetter4.TabIndex = 5;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Cursor = System.Windows.Forms.Cursors.Default;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Khaki;
            this.label46.Location = new System.Drawing.Point(37, 3);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(16, 16);
            this.label46.TabIndex = 0;
            this.label46.Text = "4";
            // 
            // pnlJackorBetter2
            // 
            this.pnlJackorBetter2.BackColor = System.Drawing.Color.Black;
            this.pnlJackorBetter2.Controls.Add(this.label28);
            this.pnlJackorBetter2.Location = new System.Drawing.Point(247, 163);
            this.pnlJackorBetter2.Name = "pnlJackorBetter2";
            this.pnlJackorBetter2.Size = new System.Drawing.Size(90, 20);
            this.pnlJackorBetter2.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Cursor = System.Windows.Forms.Cursors.Default;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Khaki;
            this.label28.Location = new System.Drawing.Point(37, 3);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(16, 16);
            this.label28.TabIndex = 0;
            this.label28.Text = "2";
            // 
            // pnlJackorBetter3
            // 
            this.pnlJackorBetter3.BackColor = System.Drawing.Color.Black;
            this.pnlJackorBetter3.Controls.Add(this.label37);
            this.pnlJackorBetter3.Location = new System.Drawing.Point(339, 163);
            this.pnlJackorBetter3.Name = "pnlJackorBetter3";
            this.pnlJackorBetter3.Size = new System.Drawing.Size(90, 20);
            this.pnlJackorBetter3.TabIndex = 5;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Cursor = System.Windows.Forms.Cursors.Default;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Khaki;
            this.label37.Location = new System.Drawing.Point(37, 3);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(16, 16);
            this.label37.TabIndex = 0;
            this.label37.Text = "3";
            // 
            // pnlJackorBetter1
            // 
            this.pnlJackorBetter1.BackColor = System.Drawing.Color.Black;
            this.pnlJackorBetter1.Controls.Add(this.label19);
            this.pnlJackorBetter1.Location = new System.Drawing.Point(155, 163);
            this.pnlJackorBetter1.Name = "pnlJackorBetter1";
            this.pnlJackorBetter1.Size = new System.Drawing.Size(90, 20);
            this.pnlJackorBetter1.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Cursor = System.Windows.Forms.Cursors.Default;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Khaki;
            this.label19.Location = new System.Drawing.Point(37, 3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 16);
            this.label19.TabIndex = 0;
            this.label19.Text = "1";
            // 
            // pnlTwopairs5
            // 
            this.pnlTwopairs5.BackColor = System.Drawing.Color.Black;
            this.pnlTwopairs5.Controls.Add(this.label54);
            this.pnlTwopairs5.Location = new System.Drawing.Point(523, 143);
            this.pnlTwopairs5.Name = "pnlTwopairs5";
            this.pnlTwopairs5.Size = new System.Drawing.Size(90, 20);
            this.pnlTwopairs5.TabIndex = 5;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Cursor = System.Windows.Forms.Cursors.Default;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Khaki;
            this.label54.Location = new System.Drawing.Point(33, 3);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(24, 16);
            this.label54.TabIndex = 0;
            this.label54.Text = "10";
            // 
            // pnlTwopairs4
            // 
            this.pnlTwopairs4.BackColor = System.Drawing.Color.Black;
            this.pnlTwopairs4.Controls.Add(this.label45);
            this.pnlTwopairs4.Location = new System.Drawing.Point(431, 143);
            this.pnlTwopairs4.Name = "pnlTwopairs4";
            this.pnlTwopairs4.Size = new System.Drawing.Size(90, 20);
            this.pnlTwopairs4.TabIndex = 5;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Cursor = System.Windows.Forms.Cursors.Default;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Khaki;
            this.label45.Location = new System.Drawing.Point(37, 3);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(16, 16);
            this.label45.TabIndex = 0;
            this.label45.Text = "8";
            // 
            // pnlTwopairs2
            // 
            this.pnlTwopairs2.BackColor = System.Drawing.Color.Black;
            this.pnlTwopairs2.Controls.Add(this.label27);
            this.pnlTwopairs2.Location = new System.Drawing.Point(247, 143);
            this.pnlTwopairs2.Name = "pnlTwopairs2";
            this.pnlTwopairs2.Size = new System.Drawing.Size(90, 20);
            this.pnlTwopairs2.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Cursor = System.Windows.Forms.Cursors.Default;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Khaki;
            this.label27.Location = new System.Drawing.Point(37, 3);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(16, 16);
            this.label27.TabIndex = 0;
            this.label27.Text = "4";
            // 
            // pnlTwopairs3
            // 
            this.pnlTwopairs3.BackColor = System.Drawing.Color.Black;
            this.pnlTwopairs3.Controls.Add(this.label36);
            this.pnlTwopairs3.Location = new System.Drawing.Point(339, 143);
            this.pnlTwopairs3.Name = "pnlTwopairs3";
            this.pnlTwopairs3.Size = new System.Drawing.Size(90, 20);
            this.pnlTwopairs3.TabIndex = 5;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Cursor = System.Windows.Forms.Cursors.Default;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Khaki;
            this.label36.Location = new System.Drawing.Point(37, 3);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(16, 16);
            this.label36.TabIndex = 0;
            this.label36.Text = "6";
            // 
            // pnlTwopairs1
            // 
            this.pnlTwopairs1.BackColor = System.Drawing.Color.Black;
            this.pnlTwopairs1.Controls.Add(this.label18);
            this.pnlTwopairs1.Location = new System.Drawing.Point(155, 143);
            this.pnlTwopairs1.Name = "pnlTwopairs1";
            this.pnlTwopairs1.Size = new System.Drawing.Size(90, 20);
            this.pnlTwopairs1.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Cursor = System.Windows.Forms.Cursors.Default;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Khaki;
            this.label18.Location = new System.Drawing.Point(37, 3);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 16);
            this.label18.TabIndex = 0;
            this.label18.Text = "2";
            // 
            // pnl3ofaKind5
            // 
            this.pnl3ofaKind5.BackColor = System.Drawing.Color.Black;
            this.pnl3ofaKind5.Controls.Add(this.label53);
            this.pnl3ofaKind5.Location = new System.Drawing.Point(523, 123);
            this.pnl3ofaKind5.Name = "pnl3ofaKind5";
            this.pnl3ofaKind5.Size = new System.Drawing.Size(90, 20);
            this.pnl3ofaKind5.TabIndex = 5;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Cursor = System.Windows.Forms.Cursors.Default;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Khaki;
            this.label53.Location = new System.Drawing.Point(33, 3);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(24, 16);
            this.label53.TabIndex = 0;
            this.label53.Text = "15";
            // 
            // pnl3ofaKind4
            // 
            this.pnl3ofaKind4.BackColor = System.Drawing.Color.Black;
            this.pnl3ofaKind4.Controls.Add(this.label44);
            this.pnl3ofaKind4.Location = new System.Drawing.Point(431, 123);
            this.pnl3ofaKind4.Name = "pnl3ofaKind4";
            this.pnl3ofaKind4.Size = new System.Drawing.Size(90, 20);
            this.pnl3ofaKind4.TabIndex = 5;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Cursor = System.Windows.Forms.Cursors.Default;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Khaki;
            this.label44.Location = new System.Drawing.Point(33, 3);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(24, 16);
            this.label44.TabIndex = 0;
            this.label44.Text = "12";
            // 
            // pnl3ofaKind2
            // 
            this.pnl3ofaKind2.BackColor = System.Drawing.Color.Black;
            this.pnl3ofaKind2.Controls.Add(this.label26);
            this.pnl3ofaKind2.Location = new System.Drawing.Point(247, 123);
            this.pnl3ofaKind2.Name = "pnl3ofaKind2";
            this.pnl3ofaKind2.Size = new System.Drawing.Size(90, 20);
            this.pnl3ofaKind2.TabIndex = 5;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Cursor = System.Windows.Forms.Cursors.Default;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Khaki;
            this.label26.Location = new System.Drawing.Point(37, 3);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(16, 16);
            this.label26.TabIndex = 0;
            this.label26.Text = "6";
            // 
            // pnl3ofaKind3
            // 
            this.pnl3ofaKind3.BackColor = System.Drawing.Color.Black;
            this.pnl3ofaKind3.Controls.Add(this.label35);
            this.pnl3ofaKind3.Location = new System.Drawing.Point(339, 123);
            this.pnl3ofaKind3.Name = "pnl3ofaKind3";
            this.pnl3ofaKind3.Size = new System.Drawing.Size(90, 20);
            this.pnl3ofaKind3.TabIndex = 5;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Cursor = System.Windows.Forms.Cursors.Default;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Khaki;
            this.label35.Location = new System.Drawing.Point(37, 3);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(16, 16);
            this.label35.TabIndex = 0;
            this.label35.Text = "9";
            // 
            // pnl3ofaKind1
            // 
            this.pnl3ofaKind1.BackColor = System.Drawing.Color.Black;
            this.pnl3ofaKind1.Controls.Add(this.label17);
            this.pnl3ofaKind1.Location = new System.Drawing.Point(155, 123);
            this.pnl3ofaKind1.Name = "pnl3ofaKind1";
            this.pnl3ofaKind1.Size = new System.Drawing.Size(90, 20);
            this.pnl3ofaKind1.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Khaki;
            this.label17.Location = new System.Drawing.Point(37, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 16);
            this.label17.TabIndex = 0;
            this.label17.Text = "3";
            // 
            // pnlStraight5
            // 
            this.pnlStraight5.BackColor = System.Drawing.Color.Black;
            this.pnlStraight5.Controls.Add(this.label52);
            this.pnlStraight5.Location = new System.Drawing.Point(523, 103);
            this.pnlStraight5.Name = "pnlStraight5";
            this.pnlStraight5.Size = new System.Drawing.Size(90, 20);
            this.pnlStraight5.TabIndex = 5;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Cursor = System.Windows.Forms.Cursors.Default;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Khaki;
            this.label52.Location = new System.Drawing.Point(33, 3);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(24, 16);
            this.label52.TabIndex = 0;
            this.label52.Text = "20";
            // 
            // pnlStraight4
            // 
            this.pnlStraight4.BackColor = System.Drawing.Color.Black;
            this.pnlStraight4.Controls.Add(this.label43);
            this.pnlStraight4.Location = new System.Drawing.Point(431, 103);
            this.pnlStraight4.Name = "pnlStraight4";
            this.pnlStraight4.Size = new System.Drawing.Size(90, 20);
            this.pnlStraight4.TabIndex = 5;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Cursor = System.Windows.Forms.Cursors.Default;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Khaki;
            this.label43.Location = new System.Drawing.Point(33, 3);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(24, 16);
            this.label43.TabIndex = 0;
            this.label43.Text = "16";
            // 
            // pnlStraight2
            // 
            this.pnlStraight2.BackColor = System.Drawing.Color.Black;
            this.pnlStraight2.Controls.Add(this.label25);
            this.pnlStraight2.Location = new System.Drawing.Point(247, 103);
            this.pnlStraight2.Name = "pnlStraight2";
            this.pnlStraight2.Size = new System.Drawing.Size(90, 20);
            this.pnlStraight2.TabIndex = 5;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Cursor = System.Windows.Forms.Cursors.Default;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Khaki;
            this.label25.Location = new System.Drawing.Point(37, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 16);
            this.label25.TabIndex = 0;
            this.label25.Text = "8";
            // 
            // pnlStraight3
            // 
            this.pnlStraight3.BackColor = System.Drawing.Color.Black;
            this.pnlStraight3.Controls.Add(this.label34);
            this.pnlStraight3.Location = new System.Drawing.Point(339, 103);
            this.pnlStraight3.Name = "pnlStraight3";
            this.pnlStraight3.Size = new System.Drawing.Size(90, 20);
            this.pnlStraight3.TabIndex = 5;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Cursor = System.Windows.Forms.Cursors.Default;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Khaki;
            this.label34.Location = new System.Drawing.Point(33, 3);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(24, 16);
            this.label34.TabIndex = 0;
            this.label34.Text = "12";
            // 
            // pnlStraight1
            // 
            this.pnlStraight1.BackColor = System.Drawing.Color.Black;
            this.pnlStraight1.Controls.Add(this.label16);
            this.pnlStraight1.Location = new System.Drawing.Point(155, 103);
            this.pnlStraight1.Name = "pnlStraight1";
            this.pnlStraight1.Size = new System.Drawing.Size(90, 20);
            this.pnlStraight1.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Khaki;
            this.label16.Location = new System.Drawing.Point(37, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 16);
            this.label16.TabIndex = 0;
            this.label16.Text = "4";
            // 
            // pnlFlush5
            // 
            this.pnlFlush5.BackColor = System.Drawing.Color.Black;
            this.pnlFlush5.Controls.Add(this.label51);
            this.pnlFlush5.Location = new System.Drawing.Point(523, 83);
            this.pnlFlush5.Name = "pnlFlush5";
            this.pnlFlush5.Size = new System.Drawing.Size(90, 20);
            this.pnlFlush5.TabIndex = 5;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Cursor = System.Windows.Forms.Cursors.Default;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Khaki;
            this.label51.Location = new System.Drawing.Point(33, 3);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(24, 16);
            this.label51.TabIndex = 0;
            this.label51.Text = "30";
            // 
            // pnlFlush4
            // 
            this.pnlFlush4.BackColor = System.Drawing.Color.Black;
            this.pnlFlush4.Controls.Add(this.label42);
            this.pnlFlush4.Location = new System.Drawing.Point(431, 83);
            this.pnlFlush4.Name = "pnlFlush4";
            this.pnlFlush4.Size = new System.Drawing.Size(90, 20);
            this.pnlFlush4.TabIndex = 5;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Cursor = System.Windows.Forms.Cursors.Default;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Khaki;
            this.label42.Location = new System.Drawing.Point(33, 3);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(24, 16);
            this.label42.TabIndex = 0;
            this.label42.Text = "24";
            // 
            // pnlFlush2
            // 
            this.pnlFlush2.BackColor = System.Drawing.Color.Black;
            this.pnlFlush2.Controls.Add(this.label24);
            this.pnlFlush2.Location = new System.Drawing.Point(247, 83);
            this.pnlFlush2.Name = "pnlFlush2";
            this.pnlFlush2.Size = new System.Drawing.Size(90, 20);
            this.pnlFlush2.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Cursor = System.Windows.Forms.Cursors.Default;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Khaki;
            this.label24.Location = new System.Drawing.Point(33, 3);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(24, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "12";
            // 
            // pnlFlush3
            // 
            this.pnlFlush3.BackColor = System.Drawing.Color.Black;
            this.pnlFlush3.Controls.Add(this.label33);
            this.pnlFlush3.Location = new System.Drawing.Point(339, 83);
            this.pnlFlush3.Name = "pnlFlush3";
            this.pnlFlush3.Size = new System.Drawing.Size(90, 20);
            this.pnlFlush3.TabIndex = 5;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Cursor = System.Windows.Forms.Cursors.Default;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Khaki;
            this.label33.Location = new System.Drawing.Point(33, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(24, 16);
            this.label33.TabIndex = 0;
            this.label33.Text = "18";
            // 
            // pnlFlush1
            // 
            this.pnlFlush1.BackColor = System.Drawing.Color.Black;
            this.pnlFlush1.Controls.Add(this.label15);
            this.pnlFlush1.Location = new System.Drawing.Point(155, 83);
            this.pnlFlush1.Name = "pnlFlush1";
            this.pnlFlush1.Size = new System.Drawing.Size(90, 20);
            this.pnlFlush1.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Khaki;
            this.label15.Location = new System.Drawing.Point(37, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 16);
            this.label15.TabIndex = 0;
            this.label15.Text = "6";
            // 
            // pnlFullHouse5
            // 
            this.pnlFullHouse5.BackColor = System.Drawing.Color.Black;
            this.pnlFullHouse5.Controls.Add(this.label50);
            this.pnlFullHouse5.Location = new System.Drawing.Point(523, 63);
            this.pnlFullHouse5.Name = "pnlFullHouse5";
            this.pnlFullHouse5.Size = new System.Drawing.Size(90, 20);
            this.pnlFullHouse5.TabIndex = 5;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Cursor = System.Windows.Forms.Cursors.Default;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Khaki;
            this.label50.Location = new System.Drawing.Point(33, 3);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(24, 16);
            this.label50.TabIndex = 0;
            this.label50.Text = "45";
            // 
            // pnlFullHouse4
            // 
            this.pnlFullHouse4.BackColor = System.Drawing.Color.Black;
            this.pnlFullHouse4.Controls.Add(this.label41);
            this.pnlFullHouse4.Location = new System.Drawing.Point(431, 63);
            this.pnlFullHouse4.Name = "pnlFullHouse4";
            this.pnlFullHouse4.Size = new System.Drawing.Size(90, 20);
            this.pnlFullHouse4.TabIndex = 5;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Cursor = System.Windows.Forms.Cursors.Default;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Khaki;
            this.label41.Location = new System.Drawing.Point(33, 3);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(24, 16);
            this.label41.TabIndex = 0;
            this.label41.Text = "36";
            // 
            // pnlFullHouse2
            // 
            this.pnlFullHouse2.BackColor = System.Drawing.Color.Black;
            this.pnlFullHouse2.Controls.Add(this.label23);
            this.pnlFullHouse2.Location = new System.Drawing.Point(247, 63);
            this.pnlFullHouse2.Name = "pnlFullHouse2";
            this.pnlFullHouse2.Size = new System.Drawing.Size(90, 20);
            this.pnlFullHouse2.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Cursor = System.Windows.Forms.Cursors.Default;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Khaki;
            this.label23.Location = new System.Drawing.Point(33, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(24, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "18";
            // 
            // pnlFullHouse3
            // 
            this.pnlFullHouse3.BackColor = System.Drawing.Color.Black;
            this.pnlFullHouse3.Controls.Add(this.label32);
            this.pnlFullHouse3.Location = new System.Drawing.Point(339, 63);
            this.pnlFullHouse3.Name = "pnlFullHouse3";
            this.pnlFullHouse3.Size = new System.Drawing.Size(90, 20);
            this.pnlFullHouse3.TabIndex = 5;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Cursor = System.Windows.Forms.Cursors.Default;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Khaki;
            this.label32.Location = new System.Drawing.Point(33, 3);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(24, 16);
            this.label32.TabIndex = 0;
            this.label32.Text = "27";
            // 
            // pnlFullHouse1
            // 
            this.pnlFullHouse1.BackColor = System.Drawing.Color.Black;
            this.pnlFullHouse1.Controls.Add(this.label14);
            this.pnlFullHouse1.Location = new System.Drawing.Point(155, 63);
            this.pnlFullHouse1.Name = "pnlFullHouse1";
            this.pnlFullHouse1.Size = new System.Drawing.Size(90, 20);
            this.pnlFullHouse1.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Khaki;
            this.label14.Location = new System.Drawing.Point(37, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "9";
            // 
            // pnl4ofaKind5
            // 
            this.pnl4ofaKind5.BackColor = System.Drawing.Color.Black;
            this.pnl4ofaKind5.Controls.Add(this.label49);
            this.pnl4ofaKind5.Location = new System.Drawing.Point(523, 43);
            this.pnl4ofaKind5.Name = "pnl4ofaKind5";
            this.pnl4ofaKind5.Size = new System.Drawing.Size(90, 20);
            this.pnl4ofaKind5.TabIndex = 5;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Cursor = System.Windows.Forms.Cursors.Default;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Khaki;
            this.label49.Location = new System.Drawing.Point(29, 3);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(32, 16);
            this.label49.TabIndex = 0;
            this.label49.Text = "125";
            // 
            // pnl4ofaKind4
            // 
            this.pnl4ofaKind4.BackColor = System.Drawing.Color.Black;
            this.pnl4ofaKind4.Controls.Add(this.label40);
            this.pnl4ofaKind4.Location = new System.Drawing.Point(431, 43);
            this.pnl4ofaKind4.Name = "pnl4ofaKind4";
            this.pnl4ofaKind4.Size = new System.Drawing.Size(90, 20);
            this.pnl4ofaKind4.TabIndex = 5;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Cursor = System.Windows.Forms.Cursors.Default;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Khaki;
            this.label40.Location = new System.Drawing.Point(29, 3);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(32, 16);
            this.label40.TabIndex = 0;
            this.label40.Text = "100";
            // 
            // pnl4ofaKind2
            // 
            this.pnl4ofaKind2.BackColor = System.Drawing.Color.Black;
            this.pnl4ofaKind2.Controls.Add(this.label22);
            this.pnl4ofaKind2.Location = new System.Drawing.Point(247, 43);
            this.pnl4ofaKind2.Name = "pnl4ofaKind2";
            this.pnl4ofaKind2.Size = new System.Drawing.Size(90, 20);
            this.pnl4ofaKind2.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Cursor = System.Windows.Forms.Cursors.Default;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Khaki;
            this.label22.Location = new System.Drawing.Point(33, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 16);
            this.label22.TabIndex = 0;
            this.label22.Text = "50";
            // 
            // pnl4ofaKind3
            // 
            this.pnl4ofaKind3.BackColor = System.Drawing.Color.Black;
            this.pnl4ofaKind3.Controls.Add(this.label31);
            this.pnl4ofaKind3.Location = new System.Drawing.Point(339, 43);
            this.pnl4ofaKind3.Name = "pnl4ofaKind3";
            this.pnl4ofaKind3.Size = new System.Drawing.Size(90, 20);
            this.pnl4ofaKind3.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Cursor = System.Windows.Forms.Cursors.Default;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Khaki;
            this.label31.Location = new System.Drawing.Point(33, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(24, 16);
            this.label31.TabIndex = 0;
            this.label31.Text = "75";
            // 
            // pnl4ofaKind1
            // 
            this.pnl4ofaKind1.BackColor = System.Drawing.Color.Black;
            this.pnl4ofaKind1.Controls.Add(this.label13);
            this.pnl4ofaKind1.Location = new System.Drawing.Point(155, 43);
            this.pnl4ofaKind1.Name = "pnl4ofaKind1";
            this.pnl4ofaKind1.Size = new System.Drawing.Size(90, 20);
            this.pnl4ofaKind1.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Khaki;
            this.label13.Location = new System.Drawing.Point(33, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "25";
            // 
            // pnlStraightFlush5
            // 
            this.pnlStraightFlush5.BackColor = System.Drawing.Color.Black;
            this.pnlStraightFlush5.Controls.Add(this.label48);
            this.pnlStraightFlush5.Location = new System.Drawing.Point(523, 23);
            this.pnlStraightFlush5.Name = "pnlStraightFlush5";
            this.pnlStraightFlush5.Size = new System.Drawing.Size(90, 20);
            this.pnlStraightFlush5.TabIndex = 5;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Cursor = System.Windows.Forms.Cursors.Default;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Khaki;
            this.label48.Location = new System.Drawing.Point(29, 3);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(32, 16);
            this.label48.TabIndex = 0;
            this.label48.Text = "250";
            // 
            // pnlStraightFlush4
            // 
            this.pnlStraightFlush4.BackColor = System.Drawing.Color.Black;
            this.pnlStraightFlush4.Controls.Add(this.label39);
            this.pnlStraightFlush4.Location = new System.Drawing.Point(431, 23);
            this.pnlStraightFlush4.Name = "pnlStraightFlush4";
            this.pnlStraightFlush4.Size = new System.Drawing.Size(90, 20);
            this.pnlStraightFlush4.TabIndex = 5;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Cursor = System.Windows.Forms.Cursors.Default;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Khaki;
            this.label39.Location = new System.Drawing.Point(29, 3);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(32, 16);
            this.label39.TabIndex = 0;
            this.label39.Text = "200";
            // 
            // pnlStraightFlush2
            // 
            this.pnlStraightFlush2.BackColor = System.Drawing.Color.Black;
            this.pnlStraightFlush2.Controls.Add(this.label21);
            this.pnlStraightFlush2.Location = new System.Drawing.Point(247, 23);
            this.pnlStraightFlush2.Name = "pnlStraightFlush2";
            this.pnlStraightFlush2.Size = new System.Drawing.Size(90, 20);
            this.pnlStraightFlush2.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Cursor = System.Windows.Forms.Cursors.Default;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Khaki;
            this.label21.Location = new System.Drawing.Point(29, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 16);
            this.label21.TabIndex = 0;
            this.label21.Text = "100";
            // 
            // pnlStraightFlush3
            // 
            this.pnlStraightFlush3.BackColor = System.Drawing.Color.Black;
            this.pnlStraightFlush3.Controls.Add(this.label30);
            this.pnlStraightFlush3.Location = new System.Drawing.Point(339, 23);
            this.pnlStraightFlush3.Name = "pnlStraightFlush3";
            this.pnlStraightFlush3.Size = new System.Drawing.Size(90, 20);
            this.pnlStraightFlush3.TabIndex = 5;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Cursor = System.Windows.Forms.Cursors.Default;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Khaki;
            this.label30.Location = new System.Drawing.Point(29, 3);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 16);
            this.label30.TabIndex = 0;
            this.label30.Text = "150";
            // 
            // pnlStraightFlush1
            // 
            this.pnlStraightFlush1.BackColor = System.Drawing.Color.Black;
            this.pnlStraightFlush1.Controls.Add(this.label12);
            this.pnlStraightFlush1.Location = new System.Drawing.Point(155, 23);
            this.pnlStraightFlush1.Name = "pnlStraightFlush1";
            this.pnlStraightFlush1.Size = new System.Drawing.Size(90, 20);
            this.pnlStraightFlush1.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Khaki;
            this.label12.Location = new System.Drawing.Point(33, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "50";
            // 
            // pnlRoyaleFlush5
            // 
            this.pnlRoyaleFlush5.BackColor = System.Drawing.Color.Black;
            this.pnlRoyaleFlush5.Controls.Add(this.label47);
            this.pnlRoyaleFlush5.Location = new System.Drawing.Point(523, 3);
            this.pnlRoyaleFlush5.Name = "pnlRoyaleFlush5";
            this.pnlRoyaleFlush5.Size = new System.Drawing.Size(90, 20);
            this.pnlRoyaleFlush5.TabIndex = 5;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Cursor = System.Windows.Forms.Cursors.Default;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Khaki;
            this.label47.Location = new System.Drawing.Point(25, 3);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(40, 16);
            this.label47.TabIndex = 0;
            this.label47.Text = "4000";
            // 
            // pnlRoyaleFlush4
            // 
            this.pnlRoyaleFlush4.BackColor = System.Drawing.Color.Black;
            this.pnlRoyaleFlush4.Controls.Add(this.label38);
            this.pnlRoyaleFlush4.Location = new System.Drawing.Point(431, 3);
            this.pnlRoyaleFlush4.Name = "pnlRoyaleFlush4";
            this.pnlRoyaleFlush4.Size = new System.Drawing.Size(90, 20);
            this.pnlRoyaleFlush4.TabIndex = 5;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Cursor = System.Windows.Forms.Cursors.Default;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Khaki;
            this.label38.Location = new System.Drawing.Point(25, 3);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(40, 16);
            this.label38.TabIndex = 0;
            this.label38.Text = "1000";
            // 
            // pnlRoyaleFlush2
            // 
            this.pnlRoyaleFlush2.BackColor = System.Drawing.Color.Black;
            this.pnlRoyaleFlush2.Controls.Add(this.label20);
            this.pnlRoyaleFlush2.Location = new System.Drawing.Point(247, 3);
            this.pnlRoyaleFlush2.Name = "pnlRoyaleFlush2";
            this.pnlRoyaleFlush2.Size = new System.Drawing.Size(90, 20);
            this.pnlRoyaleFlush2.TabIndex = 5;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Cursor = System.Windows.Forms.Cursors.Default;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Khaki;
            this.label20.Location = new System.Drawing.Point(29, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 16);
            this.label20.TabIndex = 0;
            this.label20.Text = "500";
            // 
            // pnlRoyaleFlush3
            // 
            this.pnlRoyaleFlush3.BackColor = System.Drawing.Color.Black;
            this.pnlRoyaleFlush3.Controls.Add(this.label29);
            this.pnlRoyaleFlush3.Location = new System.Drawing.Point(339, 3);
            this.pnlRoyaleFlush3.Name = "pnlRoyaleFlush3";
            this.pnlRoyaleFlush3.Size = new System.Drawing.Size(90, 20);
            this.pnlRoyaleFlush3.TabIndex = 5;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Cursor = System.Windows.Forms.Cursors.Default;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Khaki;
            this.label29.Location = new System.Drawing.Point(29, 3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(32, 16);
            this.label29.TabIndex = 0;
            this.label29.Text = "750";
            // 
            // pnlRoyaleFlush1
            // 
            this.pnlRoyaleFlush1.BackColor = System.Drawing.Color.Black;
            this.pnlRoyaleFlush1.Controls.Add(this.label11);
            this.pnlRoyaleFlush1.Location = new System.Drawing.Point(155, 3);
            this.pnlRoyaleFlush1.Name = "pnlRoyaleFlush1";
            this.pnlRoyaleFlush1.Size = new System.Drawing.Size(90, 20);
            this.pnlRoyaleFlush1.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Khaki;
            this.label11.Location = new System.Drawing.Point(29, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 16);
            this.label11.TabIndex = 0;
            this.label11.Text = "250";
            // 
            // pnlRoyaleFlush
            // 
            this.pnlRoyaleFlush.BackColor = System.Drawing.Color.Black;
            this.pnlRoyaleFlush.Controls.Add(this.label2);
            this.pnlRoyaleFlush.Location = new System.Drawing.Point(3, 3);
            this.pnlRoyaleFlush.Name = "pnlRoyaleFlush";
            this.pnlRoyaleFlush.Size = new System.Drawing.Size(150, 20);
            this.pnlRoyaleFlush.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Khaki;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "ROYALE FLUSH";
            // 
            // btnActionDAYE
            // 
            this.btnActionDAYE.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnActionDAYE.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnActionDAYE.BackgroundImage")));
            this.btnActionDAYE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnActionDAYE.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnActionDAYE.Font = new System.Drawing.Font("Playbill", 27F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActionDAYE.Location = new System.Drawing.Point(575, 761);
            this.btnActionDAYE.Name = "btnActionDAYE";
            this.btnActionDAYE.Size = new System.Drawing.Size(120, 47);
            this.btnActionDAYE.TabIndex = 2;
            this.btnActionDAYE.Text = "Deal";
            this.btnActionDAYE.UseVisualStyleBackColor = true;
            this.btnActionDAYE.Click += new System.EventHandler(this.btnActionDAYE_Click);
            // 
            // nudBetDAYE
            // 
            this.nudBetDAYE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nudBetDAYE.BackColor = System.Drawing.Color.Khaki;
            this.nudBetDAYE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudBetDAYE.Font = new System.Drawing.Font("Playbill", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudBetDAYE.Location = new System.Drawing.Point(79, 761);
            this.nudBetDAYE.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudBetDAYE.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBetDAYE.Name = "nudBetDAYE";
            this.nudBetDAYE.ReadOnly = true;
            this.nudBetDAYE.Size = new System.Drawing.Size(74, 39);
            this.nudBetDAYE.TabIndex = 3;
            this.nudBetDAYE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudBetDAYE.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBetDAYE.ValueChanged += new System.EventHandler(this.nudBetDAYE_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Playbill", 36F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(1105, 756);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 49);
            this.label1.TabIndex = 4;
            this.label1.Text = "Credits:";
            // 
            // lblCreditsDAYE
            // 
            this.lblCreditsDAYE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCreditsDAYE.AutoSize = true;
            this.lblCreditsDAYE.BackColor = System.Drawing.Color.Transparent;
            this.lblCreditsDAYE.Font = new System.Drawing.Font("Playbill", 36F, System.Drawing.FontStyle.Bold);
            this.lblCreditsDAYE.Location = new System.Drawing.Point(1214, 756);
            this.lblCreditsDAYE.Name = "lblCreditsDAYE";
            this.lblCreditsDAYE.Size = new System.Drawing.Size(40, 49);
            this.lblCreditsDAYE.TabIndex = 4;
            this.lblCreditsDAYE.Text = "0";
            // 
            // TimerDeal
            // 
            this.TimerDeal.Interval = 200;
            this.TimerDeal.Tick += new System.EventHandler(this.TimerDeal_Tick);
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.pbCardholder5DAYE);
            this.panel2.Controls.Add(this.pbCardholder4DAYE);
            this.panel2.Controls.Add(this.pbCardholder3DAYE);
            this.panel2.Controls.Add(this.pbCardholder2DAYE);
            this.panel2.Controls.Add(this.pbCardholder1DAYE);
            this.panel2.Location = new System.Drawing.Point(17, 241);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1250, 478);
            this.panel2.TabIndex = 5;
            // 
            // pbCardholder5DAYE
            // 
            this.pbCardholder5DAYE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCardholder5DAYE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(47)))));
            this.pbCardholder5DAYE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbCardholder5DAYE.Location = new System.Drawing.Point(949, 106);
            this.pbCardholder5DAYE.Name = "pbCardholder5DAYE";
            this.pbCardholder5DAYE.Size = new System.Drawing.Size(180, 278);
            this.pbCardholder5DAYE.TabIndex = 2;
            this.pbCardholder5DAYE.TabStop = false;
            // 
            // pbCardholder4DAYE
            // 
            this.pbCardholder4DAYE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCardholder4DAYE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(47)))));
            this.pbCardholder4DAYE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbCardholder4DAYE.Location = new System.Drawing.Point(744, 106);
            this.pbCardholder4DAYE.Name = "pbCardholder4DAYE";
            this.pbCardholder4DAYE.Size = new System.Drawing.Size(180, 278);
            this.pbCardholder4DAYE.TabIndex = 3;
            this.pbCardholder4DAYE.TabStop = false;
            // 
            // pbCardholder3DAYE
            // 
            this.pbCardholder3DAYE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCardholder3DAYE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(47)))));
            this.pbCardholder3DAYE.Location = new System.Drawing.Point(539, 106);
            this.pbCardholder3DAYE.Name = "pbCardholder3DAYE";
            this.pbCardholder3DAYE.Size = new System.Drawing.Size(180, 278);
            this.pbCardholder3DAYE.TabIndex = 4;
            this.pbCardholder3DAYE.TabStop = false;
            // 
            // pbCardholder2DAYE
            // 
            this.pbCardholder2DAYE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCardholder2DAYE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(47)))));
            this.pbCardholder2DAYE.Location = new System.Drawing.Point(334, 106);
            this.pbCardholder2DAYE.Name = "pbCardholder2DAYE";
            this.pbCardholder2DAYE.Size = new System.Drawing.Size(180, 278);
            this.pbCardholder2DAYE.TabIndex = 5;
            this.pbCardholder2DAYE.TabStop = false;
            // 
            // pbCardholder1DAYE
            // 
            this.pbCardholder1DAYE.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbCardholder1DAYE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(91)))), ((int)(((byte)(47)))));
            this.pbCardholder1DAYE.Location = new System.Drawing.Point(129, 106);
            this.pbCardholder1DAYE.Name = "pbCardholder1DAYE";
            this.pbCardholder1DAYE.Size = new System.Drawing.Size(180, 278);
            this.pbCardholder1DAYE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbCardholder1DAYE.TabIndex = 6;
            this.pbCardholder1DAYE.TabStop = false;
            // 
            // label56
            // 
            this.label56.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Playbill", 36F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(12, 756);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(70, 49);
            this.label56.TabIndex = 4;
            this.label56.Text = "Bet:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(52, 36);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(101, 31);
            this.label57.TabIndex = 6;
            this.label57.Text = "label57";
            // 
            // PokerGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1292, 814);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.lblCreditsDAYE);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudBetDAYE);
            this.Controls.Add(this.btnActionDAYE);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1308, 759);
            this.Name = "PokerGame";
            this.Text = "Poker";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PokerGame_Load);
            this.panel1.ResumeLayout(false);
            this.pnlJackorBetter.ResumeLayout(false);
            this.pnlJackorBetter.PerformLayout();
            this.pnlTwopairs.ResumeLayout(false);
            this.pnlTwopairs.PerformLayout();
            this.pnl3ofaKind.ResumeLayout(false);
            this.pnl3ofaKind.PerformLayout();
            this.pnlStraight.ResumeLayout(false);
            this.pnlStraight.PerformLayout();
            this.pnlFlush.ResumeLayout(false);
            this.pnlFlush.PerformLayout();
            this.pnlFullHouse.ResumeLayout(false);
            this.pnlFullHouse.PerformLayout();
            this.pnl4ofaKind.ResumeLayout(false);
            this.pnl4ofaKind.PerformLayout();
            this.pnlStraightFlush.ResumeLayout(false);
            this.pnlStraightFlush.PerformLayout();
            this.pnlJackorBetter5.ResumeLayout(false);
            this.pnlJackorBetter5.PerformLayout();
            this.pnlJackorBetter4.ResumeLayout(false);
            this.pnlJackorBetter4.PerformLayout();
            this.pnlJackorBetter2.ResumeLayout(false);
            this.pnlJackorBetter2.PerformLayout();
            this.pnlJackorBetter3.ResumeLayout(false);
            this.pnlJackorBetter3.PerformLayout();
            this.pnlJackorBetter1.ResumeLayout(false);
            this.pnlJackorBetter1.PerformLayout();
            this.pnlTwopairs5.ResumeLayout(false);
            this.pnlTwopairs5.PerformLayout();
            this.pnlTwopairs4.ResumeLayout(false);
            this.pnlTwopairs4.PerformLayout();
            this.pnlTwopairs2.ResumeLayout(false);
            this.pnlTwopairs2.PerformLayout();
            this.pnlTwopairs3.ResumeLayout(false);
            this.pnlTwopairs3.PerformLayout();
            this.pnlTwopairs1.ResumeLayout(false);
            this.pnlTwopairs1.PerformLayout();
            this.pnl3ofaKind5.ResumeLayout(false);
            this.pnl3ofaKind5.PerformLayout();
            this.pnl3ofaKind4.ResumeLayout(false);
            this.pnl3ofaKind4.PerformLayout();
            this.pnl3ofaKind2.ResumeLayout(false);
            this.pnl3ofaKind2.PerformLayout();
            this.pnl3ofaKind3.ResumeLayout(false);
            this.pnl3ofaKind3.PerformLayout();
            this.pnl3ofaKind1.ResumeLayout(false);
            this.pnl3ofaKind1.PerformLayout();
            this.pnlStraight5.ResumeLayout(false);
            this.pnlStraight5.PerformLayout();
            this.pnlStraight4.ResumeLayout(false);
            this.pnlStraight4.PerformLayout();
            this.pnlStraight2.ResumeLayout(false);
            this.pnlStraight2.PerformLayout();
            this.pnlStraight3.ResumeLayout(false);
            this.pnlStraight3.PerformLayout();
            this.pnlStraight1.ResumeLayout(false);
            this.pnlStraight1.PerformLayout();
            this.pnlFlush5.ResumeLayout(false);
            this.pnlFlush5.PerformLayout();
            this.pnlFlush4.ResumeLayout(false);
            this.pnlFlush4.PerformLayout();
            this.pnlFlush2.ResumeLayout(false);
            this.pnlFlush2.PerformLayout();
            this.pnlFlush3.ResumeLayout(false);
            this.pnlFlush3.PerformLayout();
            this.pnlFlush1.ResumeLayout(false);
            this.pnlFlush1.PerformLayout();
            this.pnlFullHouse5.ResumeLayout(false);
            this.pnlFullHouse5.PerformLayout();
            this.pnlFullHouse4.ResumeLayout(false);
            this.pnlFullHouse4.PerformLayout();
            this.pnlFullHouse2.ResumeLayout(false);
            this.pnlFullHouse2.PerformLayout();
            this.pnlFullHouse3.ResumeLayout(false);
            this.pnlFullHouse3.PerformLayout();
            this.pnlFullHouse1.ResumeLayout(false);
            this.pnlFullHouse1.PerformLayout();
            this.pnl4ofaKind5.ResumeLayout(false);
            this.pnl4ofaKind5.PerformLayout();
            this.pnl4ofaKind4.ResumeLayout(false);
            this.pnl4ofaKind4.PerformLayout();
            this.pnl4ofaKind2.ResumeLayout(false);
            this.pnl4ofaKind2.PerformLayout();
            this.pnl4ofaKind3.ResumeLayout(false);
            this.pnl4ofaKind3.PerformLayout();
            this.pnl4ofaKind1.ResumeLayout(false);
            this.pnl4ofaKind1.PerformLayout();
            this.pnlStraightFlush5.ResumeLayout(false);
            this.pnlStraightFlush5.PerformLayout();
            this.pnlStraightFlush4.ResumeLayout(false);
            this.pnlStraightFlush4.PerformLayout();
            this.pnlStraightFlush2.ResumeLayout(false);
            this.pnlStraightFlush2.PerformLayout();
            this.pnlStraightFlush3.ResumeLayout(false);
            this.pnlStraightFlush3.PerformLayout();
            this.pnlStraightFlush1.ResumeLayout(false);
            this.pnlStraightFlush1.PerformLayout();
            this.pnlRoyaleFlush5.ResumeLayout(false);
            this.pnlRoyaleFlush5.PerformLayout();
            this.pnlRoyaleFlush4.ResumeLayout(false);
            this.pnlRoyaleFlush4.PerformLayout();
            this.pnlRoyaleFlush2.ResumeLayout(false);
            this.pnlRoyaleFlush2.PerformLayout();
            this.pnlRoyaleFlush3.ResumeLayout(false);
            this.pnlRoyaleFlush3.PerformLayout();
            this.pnlRoyaleFlush1.ResumeLayout(false);
            this.pnlRoyaleFlush1.PerformLayout();
            this.pnlRoyaleFlush.ResumeLayout(false);
            this.pnlRoyaleFlush.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBetDAYE)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder5DAYE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder4DAYE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder3DAYE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder2DAYE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCardholder1DAYE)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnActionDAYE;
        private System.Windows.Forms.NumericUpDown nudBetDAYE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCreditsDAYE;
        private System.Windows.Forms.Panel pnlRoyaleFlush;
        private System.Windows.Forms.Panel pnlJackorBetter;
        private System.Windows.Forms.Panel pnlTwopairs;
        private System.Windows.Forms.Panel pnl3ofaKind;
        private System.Windows.Forms.Panel pnlStraight;
        private System.Windows.Forms.Panel pnlFlush;
        private System.Windows.Forms.Panel pnlFullHouse;
        private System.Windows.Forms.Panel pnl4ofaKind;
        private System.Windows.Forms.Panel pnlStraightFlush;
        private System.Windows.Forms.Panel pnlJackorBetter5;
        private System.Windows.Forms.Panel pnlJackorBetter4;
        private System.Windows.Forms.Panel pnlJackorBetter2;
        private System.Windows.Forms.Panel pnlJackorBetter3;
        private System.Windows.Forms.Panel pnlJackorBetter1;
        private System.Windows.Forms.Panel pnlTwopairs5;
        private System.Windows.Forms.Panel pnlTwopairs4;
        private System.Windows.Forms.Panel pnlTwopairs2;
        private System.Windows.Forms.Panel pnlTwopairs3;
        private System.Windows.Forms.Panel pnlTwopairs1;
        private System.Windows.Forms.Panel pnl3ofaKind5;
        private System.Windows.Forms.Panel pnl3ofaKind4;
        private System.Windows.Forms.Panel pnl3ofaKind2;
        private System.Windows.Forms.Panel pnl3ofaKind3;
        private System.Windows.Forms.Panel pnl3ofaKind1;
        private System.Windows.Forms.Panel pnlStraight5;
        private System.Windows.Forms.Panel pnlStraight4;
        private System.Windows.Forms.Panel pnlStraight2;
        private System.Windows.Forms.Panel pnlStraight3;
        private System.Windows.Forms.Panel pnlStraight1;
        private System.Windows.Forms.Panel pnlFlush5;
        private System.Windows.Forms.Panel pnlFlush4;
        private System.Windows.Forms.Panel pnlFlush2;
        private System.Windows.Forms.Panel pnlFlush3;
        private System.Windows.Forms.Panel pnlFlush1;
        private System.Windows.Forms.Panel pnlFullHouse5;
        private System.Windows.Forms.Panel pnlFullHouse4;
        private System.Windows.Forms.Panel pnlFullHouse2;
        private System.Windows.Forms.Panel pnlFullHouse3;
        private System.Windows.Forms.Panel pnlFullHouse1;
        private System.Windows.Forms.Panel pnl4ofaKind5;
        private System.Windows.Forms.Panel pnl4ofaKind4;
        private System.Windows.Forms.Panel pnl4ofaKind2;
        private System.Windows.Forms.Panel pnl4ofaKind3;
        private System.Windows.Forms.Panel pnl4ofaKind1;
        private System.Windows.Forms.Panel pnlStraightFlush5;
        private System.Windows.Forms.Panel pnlStraightFlush4;
        private System.Windows.Forms.Panel pnlStraightFlush2;
        private System.Windows.Forms.Panel pnlStraightFlush3;
        private System.Windows.Forms.Panel pnlStraightFlush1;
        private System.Windows.Forms.Panel pnlRoyaleFlush5;
        private System.Windows.Forms.Panel pnlRoyaleFlush4;
        private System.Windows.Forms.Panel pnlRoyaleFlush2;
        private System.Windows.Forms.Panel pnlRoyaleFlush3;
        private System.Windows.Forms.Panel pnlRoyaleFlush1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Timer TimerDeal;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pbCardholder5DAYE;
        private System.Windows.Forms.PictureBox pbCardholder4DAYE;
        private System.Windows.Forms.PictureBox pbCardholder3DAYE;
        private System.Windows.Forms.PictureBox pbCardholder2DAYE;
        private System.Windows.Forms.PictureBox pbCardholder1DAYE;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
    }
}

